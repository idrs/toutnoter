import m from "mithril";
import Notes from "../models/Notes";
import Visu from "./Visu";
import Liste from "./Liste";

import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}



function TagView(vnode) {
  
  var visible = 20;
  var route;
  return {
    onbeforeremove: function(){
      Notes.dernierePage = {type:"tag",name:route[2]};
    },
    view: function(vnode) {
      route = m.route.get().split('/').map((str)=>decodeURIComponent(str));
      console.log("route?",route);
      var noteId;
      if(route.length>3){
        vnode.attrs.id = route[4];
      }
      return        m(".column.is-10.section",m(Liste,{tagName:route[2]}))
        //m(".column.is-8",m(Visu,{noteId:route[4]}))
      
    }
  }
};

module.exports = TagView;
