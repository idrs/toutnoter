import m from "mithril";
import Notes from "../models/Notes"
import Stream from "mithril-stream";
//import range from 'underscore/modules/range';
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

//import throttle from 'underscore/modules/throttle';
//import debounce from 'underscore/modules/debounce';



function Visu(vnode) {
  var id = vnode.attrs.id;
  var note = Notes.liste[id];
  console.log("note:",note)
  var titre = Stream(note.title);
  var texte = Stream(note.content);
  var tags = Stream(note.tags.filter((tag)=>!Notes.config.specialTags.includes(tag)));
  
  var specialTagsStreams = Notes.config.specialTags.reduce((acc,t)=>{acc[t]=Stream(note.tags.includes(t));return acc},{});
  var specialTags = Stream.merge(Object.values(specialTagsStreams));
  var changes = Stream.merge([titre,texte,tags,specialTags]).map(()=>true);
  changes(false);
 /* function toggleTag(t){
    var sTagArray = specialTags(); 
    if(sTagArray.includes(t)){
      specialTags(sTagArray.filter((e)=>!(e==t)));
    }
    else{
      sTagArray.push(t);
      specialTags(sTagArray);
    }
  }*/
  return {
    view: function(vnode) {
      return m('.sectione',
        m("#tags",[specialTags().map((tgBool,idx)=>tgBool?m('.chip.bg-success',Notes.config.specialTags[idx]):null),tags().map((tg)=>m('.chip',tg))]),
        m('.content',m.trust(md2html("### "+titre()+'\n'+texte()))))
    }
  }

}

module.exports = Visu 