
import m from "mithril";
import Notes from "../models/Notes"
import Stream from "mithril-stream";
//import range from 'underscore/modules/range';
import marked from "../models/Marked";
import dompurify from "dompurify";
import fm from "front-matter";




function md2html(txt) {
  const { attributes, body } = fm(txt);
  console.log("attributes de front-matter:",attributes);
  return dompurify.sanitize(marked.parse(body));
}


function Lapo(vnode) {
    var id = vnode.attrs.id;



    var textesParPage = Notes.note.texte().split(/--page\d*-- *\n/);
    
    //param par défaut
    var param = {format:'a4'}; 
    
    //lecture param dans en-tête
    textesParPage[0].split('\n').forEach(element => {
        var [name,value] = element.split(':').map((t)=>t.trim())
        param[name]=value;
    });

    //paramètrage format
    var niveaux = 6 - param.format.substring(1);
   /* function toggleTag(t){
      var sTagArray = specialTags(); 
      if(sTagArray.includes(t)){
        specialTags(sTagArray.filter((e)=>!(e==t)));
      }
      else{
        sTagArray.push(t);
        specialTags(sTagArray);
      }
    }*/

    

    return {
      oninit: function(){Notes.loadNote(vnode.attrs.id);},
      view: function(vnode) {

        var textesParPage = Notes.note.texte().split("--page--\n");
        console.log(textesParPage)
    //param par défaut
    var param = {format:'a4'}; 
    
    //lecture param dans en-tête
    textesParPage[0].split('\n').forEach(element => {
        var [name,value] = element.split(':').map((t)=>t.trim())
        param[name]=value;
    });

    //paramètrage format
    var niveaux = Math.min(6 - param.format.substring(1),textesParPage.length-3);
    var vue = [
      m('.l0',
          m('.pageTitre',
          m(".titreA.gauche",m.trust("LA PO <br> LA PO")),
          m(".poemeTitre",m.trust(md2html(textesParPage[1])))
      )),
      m('.l0.cB',
      m('.pageTitre',
       
          m(".titreA.droite",m.trust("ésie dans <br> che")),
          m(".poemeTitre",m.trust(md2html(textesParPage[2])))
      ))
  ]


  for(var i=1;i<=niveaux;i++){
      vue = [
          m('.l'+i+".cA",m(".rotate",vue)),
          m('.l'+i+".cB",
              m(".poeme",m.trust(md2html(textesParPage[i+2])))
          )
      ]
  }
        return m('.content',
          m('.lapolapo',vue))
      }
    }
  }
  
  module.exports = Lapo