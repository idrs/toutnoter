import m from "mithril";
import Liste from "./Liste";
import ListePartielle from "./ListePartielle";

import Notes from "../models/Notes";

function Accueil(vnode) {
  //var whereami = m.route.get();
  return {
    onbeforeremove: function(){
      Notes.dernierePage = {type:"accueil"};
    },
    view: function(vnode){
      //console.log("vnode.attrs",vnode.attrs);
      return  m(".container",
          /*Notes.config.specialTags.map((tag)=>
          m(".container",
            [m("h4",tag),m(Liste,{tagName:tag}),m(".divider")]
          )),*/
          m(".container",
          m('h4',"Toutes les notes"),
          m(Liste)
          )
        )
      
    }
  }
};

module.exports = Accueil;
