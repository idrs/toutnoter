import m from "mithril";
import Notes from "../models/Notes";
//import Editeur from "./Editeur";
//import Visu from "./Visu";

import uFuzzy from '@leeoniya/ufuzzy';


let opts = {};

let uf = new uFuzzy(opts);

import Stream from "mithril-stream";

import throttle from 'underscore/modules/throttle';

import marked from "../models/Marked";
import dompurify from "dompurify";
import fm from "front-matter";


function md2html(txt) {
  const { attributes, body } = fm(txt);
  console.log("attributes de front-matter:",attributes);
  return dompurify.sanitize(marked.parse(body));
}

function md2htmlInline(txt) {
  const { attributes, body } = fm(txt);
  console.log("attributes de front-matter:",attributes);
  return dompurify.sanitize(marked.parseInline(body));
}


//import range from 'underscore/modules/range';


//import throttle from 'underscore/modules/throttle';
//import debounce from 'underscore/modules/debounce';



function Note(vnode,mode) {

    //mode d'affichage
  
    var noteId;
   
    
  
    
    //variable pour déterminer s'il faut enregistrer.
    
    //var sauvegardes = Stream.combine((a,b)=>,changes);
    //
    
    //paramètre de recherche de titre pour les liens
    var suggestion = [];
    var haystack //= Object.values(Notes.liste).map((elt)=>elt.title);
    //var keystack = Object.keys(Notes.liste);
    function chercheTitre(needle,haystack){
      [idxs, info, order] = uf.search(haystack, needle, outOfOrder = 0, infoThresh = 1e3);
      //console.log("idxs,info,order",idxs, info, order)
      //console.log(idxs.map((i)=>[keystack[i],haystack[i]]))
      suggestion = idxs.map((i)=>haystack[i])
    }
    var debutPosition;
    var finPosition;
    var linkRe = /\[\[[^\]]*\]\]/g
    var cursorIdx = Stream(0);
    var xcaret,ycaret;
      //composante éditeur
    function Editeur()  {
      return {        
        /*oninit:function(){
          Notes.config.specialTags.forEach((t)=>{specialTagsStreams[t](Notes.note.tags().split(',').map((str)=>str.trim()).includes(t))})
        },*/  
        view: function(vnode) {
            //console.log(vnode.attrs);
            if (Object.values(Notes.liste).length == 0 || Notes.liste[vnode.attrs
                .id] === undefined) {
              setTimeout(m.redraw, 300);
              /*if (Notes.liste[id] === undefined) {
                console.log("is undefine ?")
              }*/
              return m(".enAttente.section", "En attente de chargement...")
            }
            else {
      
              //console.log(texte(),titre(),changes())
              //console.log("specialTags",Notes.specialTags.map((t)=>t+specialTagsStreams[t]()))
              return m("#editeur",[
                /*m('.btn#boutonSave',{
                  onclick:()=>{ 
                    changes(false);
                    enregistre();
                  }
                },changes()?"Enregistrer":"Rien à enregistrer" ),*/
               /* m("#titre", 
                  m("input[type=text].form-input.text-bold.text-large.my-1", {
                    oninput: function (e) { titre(e.target.value); },
                    value: titre()
                  })
                ),*/
                m("#titreChamp",m("#titre", 
            m("input[type=text].form-input.text-bold.text-large.my-1", {
              oninput: function (e) { Notes.note.titre(e.target.value); },
              value: Notes.note.titre(),
              placeholder:"titre"
            }))),
                m("span#specialTags",Notes.config.specialTags.map(function(t){
                  return m("span.chip"+(Notes.note.specialTagsStreams[t]()?".bg-success":""),{
                      onclick:()=>Notes.note.specialTagsStreams[t](!Notes.note.specialTagsStreams[t]())
                    },
                  t)
                })),
                m("span#tags",m("input[type=text].form-input.my-1", {
                  placeholder: "tags",
                  oninput: function (e) { Notes.note.tags(e.target.value) },
                  value: Notes.note.tags()
                })
                ),
                m("#texte", 
                  m("textarea#areatexte.form-input.my-1", {
                    oninput: function (e) { 
                      console.log(e)
                      var text = e.target.value;
                      cursorIdx(e.target.selectionStart);
                      var stringTilCursor =  text.slice(0,cursorIdx())
                      var arrayOfReturns = stringTilCursor.match(/\n/g);
                      var rows = (arrayOfReturns || []).length;
                      var lastReturn = stringTilCursor.lastIndexOf("\n");
                      var cols = cursorIdx()-lastReturn;
                      //console.log(arrayOfReturns)
                      //console.log("r,c=",rows,cols);
                      
                      if(e.data=="["){
                        text = text.slice(0,cursorIdx())+"]"+text.slice(cursorIdx())
                        e.target.value = text
                        
                      } 
                      Notes.note.texte(e.target.value);
                      e.target.selectionStart = e.target.selectionEnd = cursorIdx();
                      suggestion = [];
                      array1 = linkRe.exec(text)
                      while ( array1!== null) {
                        if(array1.index<cursorIdx() && linkRe.lastIndex>=cursorIdx()){
                          console.log("cherche dans les titres")
                          chercheTitre(array1[0].slice(2,-2),haystack);
                          debutPosition = array1.index+2;
                          finPosition = linkRe.lastIndex-2;
                          break;
                        }
                        array1 = linkRe.exec(text)
                      }
                    },
                    placeholder: "contenu",
                    value: Notes.note.texte()
                  }),
                  m("ul.menu#suggestion"+((suggestion.length==0)?".d-hide":""),
                    {style:"position:absolute;top:"+ycaret+"px;left:"+xcaret+"px;"}
                  ,
                    suggestion.map((t)=>m("li.menu-item",{onclick:()=>{
                      var txt = Notes.note.texte()
                      Notes.note.texte(txt.substring(0,debutPosition)+t+txt.substring(finPosition));
                      suggestion = [];}
                    },t))
                  )
                ),
                m("#parent",)
              ])
            }
          }
        }
      }
      

      //composante visualiseur
      var Visu = {
          view: function(vnode) {
            var dateModif = new Date(Notes.note.modif());
            return m('.sectione',
              m("#titreChamp",m("h1",m.trust(md2htmlInline(Notes.note.titre())))),
              m(".lastEdit","modifié le "+dateModif.toLocaleDateString()),
              m("#tags.m-1",[Notes.config.specialTags.map((tg)=>Notes.note.specialTagsStreams[tg]()?m('.chip.bg-success',tg):null),Notes.note.tags().split(",").filter((tg)=>tg.trim().length>0).map((tg)=>m('.chip.active',tg.trim()))]),
              m('.content',{ondoubleclick:Notes.toggleMode},m.trust(md2html(Notes.note.texte()))))
          }
        }
      
      
    return {
      oninit: function(vnode){
         // console.log(vnode.attrs.id)
         if(!Notes.liste || Notes.liste.length == 0){
          console.log("loadNotes from Note");

          Notes.loadNotes().then(()=>{console.log("layout init end");
          Notes.loadNote(vnode.attrs.id);
          haystack = Object.values(Notes.liste).map((elt)=>elt.title);
          m.redraw()})}
        else{
          Notes.loadNote(vnode.attrs.id);
          haystack = Object.values(Notes.liste).map((elt)=>elt.title);
        }
          
          
          //charge(vnode.attrs.id);
      /*    noteId = vnode.attrs.id;
          console.log(Notes.liste[noteId]);
      titre(Notes.liste[noteId].title); 
      texte(Notes.liste[noteId].);
      tags(Notes.liste[noteId].tags.filter((tag)=>!Notes.config.specialTags.includes(tag)).join(", "));
      specialTagsStreams = Notes.config.specialTags.reduce((acc,t)=>{acc[t]=Stream(Notes.liste[noteId].tags.includes(t));return acc},{});
      //console.log(specialTagsStreams)
      specialTags = Stream.merge(Object.values(specialTagsStreams));*/
        //  console.log("titre,texte et tags",Notes.note.titre(),Notes.note.texte(),Notes.note.tags());
        //  console.log("Note init end")
        },
        onbeforeremove: function(){
          Notes.dernierePage = {type:"note",name:vnode.attrs.id};
        },
      view: function(vnode) {
        //console.log(vnode.attrs);
        //fonction d'enregistrement
        

        if (!Notes.note) {
            //setTimeout(m.redraw, 50);
            /*if (Notes.liste[id] === undefined) {
            console.log("is undefine ?")
            }*/
            return m(".enAttente", "Chargement...")
        }
        else { 
          var id = vnode.attrs.id;
          var note = Notes.liste[id];
          vnode.attrs.type = note.type;
          vnode.attrs.note = note;
          //console.log(id,nson)
          /*console.log(sonnet.content
                          .split(/\r?\n/)
                          .slice(14)
                                                      .reduce((acc,l)=>
                              (l.length==0)?[...acc,l]:[...acc.slice(0,acc.length-1),acc[acc.length-1]+'\n'+l],[""])

                      )*/
          //console.log(premVersStrophe)
          var Comp = (Notes.note.mode=="view"?Visu:Editeur);
          //console.log(Comp)
          return m("#note.grid-lg", m(Comp,vnode.attrs));
        }
      }
    }
  }
  
  
  module.exports = Note;
