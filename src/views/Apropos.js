
import m from "mithril";
import Notes from "../models/Notes"

function Apropos() {
  
  var  fonts = [
    "Ouroboros by Ariel Martín Pérez, with the contribution of H·Alix Sanyas. Distributed by velvetyne.fr.",
  ]

  return {
    view: function(vnode) {
      
        return m(".container.is-max-desktop.box",
          [
           m(".fontCredit","Polices d'écriture:"),
           m("ul.fontCredit",fonts.map((el)=>m('li',el)))

        ]
          )
        }
      }
    }
  
  
  
  module.exports = Apropos


//   