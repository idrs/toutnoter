import m, { route } from "mithril"
import Notes from "../models/Notes"
import { negate } from "underscore";



function Layout(vnode) {
  var modal = Notes.isLoading?m(".loader","En attente..."):null;
  //await Notes.loadNotes();
  //console.log(vnode.attrs);
  
  //var isActive = false;
  //const toggleActiveMenu = ()=> isActive = !isActive;
  var showSide = false;
  var togglesidebar = ()=> showSide = !showSide;  
  var getTagsArray = (note)=> {
    var arr = Notes.config.specialTags.filter((tg)=>note.specialTagsStreams[tg]());
    return arr.concat(note.tags().split(',').map((t)=>t.trim()));
  };
  var modal = false;
  function toggleModal(){
    modal = !modal;
  }
  console.log(Notes.config);

  return {
    oninit: function(){console.log("layout init");showSide = false;
    },
    onremove: function() {
      console.log('->onremove:')
      showSide = false; 
  },
    view: function(vnode){
      //console.log(vnode)
      
      var routeActuelle = decodeURIComponent(m.route.get()).split("/");
      var typePage = routeActuelle[1];
      var idOuTag = routeActuelle[2];
      //console.log(routeActuelle)
      var voir = true;
      console.log("tags: ",Notes.note?Notes.note.tags():"pas de note...")
     /* if (Object.values(Notes.liste).length == 0 ) {
      setTimeout(m.redraw, 50);
      
      return m(".enAttente.section", "En attente de chargement...")
    }*/
      return m("#all.off-canvas.off-canvas-sidebar-show",
      //m('a#menuLink.menu-link',{onclick:function(e){e.preventDefault();toggleActiveMenu()}},m("span","")),
      m("header.navbar.container",
      m("section.navbar-section",
        m("a.off-canvas-toggle.btn.btn-primary.btn-action",{onclick:togglesidebar},
          m("i.fa-solid.fa-bars"))),
      m('section.navbar-section',
       ),
      m('section.navbar-section',((typePage=="note")?[
        m("button.btn.abs-droite",{onclick:()=>Notes.toggleMode()},
          (Notes.note&&Notes.note.mode=="view")?m("i.fa-regular.fa-pen-to-square"):m("i.fa-regular.fa-eye")), 
        ((Notes.note && getTagsArray(Notes.note).includes("lapo"))?m("button.btn.abs-droite",{onclick:()=>m.route.set("/lapo/"+Notes.note.id)},"LAPO"):null),
        (Notes.note?m("button.btn.abs-droite",{onclick:()=>toggleModal()},m("i.fa-regular.fa-square-minus")):null)]:[] 
      ))
      ),
      m('main',
      m(".modal.modal-sm"+(modal?".active":""),
        m("a.modal-overlay",{onclick:toggleModal,"aria-label":"Close"}),
        m(".modal-container",
          m(".modal-header","Effacer la note",m("a.btn.btn-clear.float-right",{onclick:()=>toggleModal(),"aria-label":"Close"})),
          m(".modal-body",m("p","Êtes-vous sûr de vouloir effacer la note ?")),
          m(".bouton.modal-footer",m("button.btn.abs-droite",{onclick:()=>{Notes.effaceNote().then(()=>{toggleModal();})}},"Ok"))
        )
      ),
      m(".aside.bg-light.off-canvas-sidebar"+(showSide?'.active':''),        
        m('ul.nav.p-2',{onclick: ()=>togglesidebar() }, 
          m('li.nav-item', m(m.route.Link,{["class"]:"text-capitalize p-centered",href:"/",},Notes.config.titre)),
          m('li.nav-item',m("a.btn",{onclick:function(){
            Notes.nouvelleNote({tags:idOuTag?[idOuTag]:[]}).then((nouvId)=>{
            m.route.set("/note/"+nouvId)})  
          }},"+ Nouvelle note")),
          Notes.config.specialTags.map((t)=>m('li.nav-item'+(t==idOuTag?'.active':''),m(m.route.Link,{href:"/tag/"+t},t))),
          m('li.nav-item'+(typePage=='tags'?'.active':''),m(m.route.Link,{href:"/tags"},"Tous les tags")),
          m("li.nav-item"+(typePage=='config'?'.active':''),m(m.route.Link,{href:"/config"},"Configuration")),

          m("li.nav-item"+(typePage=='apropos'?'.active':''),m(m.route.Link,{href:"/apropos"},"A propos")))       
        
      ),
        m("a.off-canvas-overlay",{onclick:togglesidebar})
      ,
        m("#principal.off-canvas-content",{viewer:voir},      
          vnode.children))
      )
    }
  }
}


module.exports = Layout

