import m from "mithril";
import Notes from "../models/Notes"
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

const debug = false;


function ListeTag(vnode) {
        
  
  
  return {    
    oninit:function(){
    if(!Notes.liste || Notes.liste.length == 0){
      console.log("loadNotes from Liste");
      return Notes.loadNotes().then(()=>{console.log("layout init end");m.redraw()})}
  },
  onbeforeremove: function(){
    Notes.dernierePage = {type:"tags"};
  },
    view: function() {
      const TagSet = {};
      if(!Notes.liste){return m('.loading',"Chargement");}
      Object.values(Notes.liste).forEach((n)=>  n.tags.forEach(element => {TagSet[element]=(TagSet[element]||0)+1}));
      console.log("liste parcourue")

      return m('.container.text-center.m-2',Object.keys(TagSet).map((k)=>m(m.route.Link,{"class":"btn badge m-2",href:"/tag/"+k,"data-badge":TagSet[k]},k)));
    }
  }
};
module.exports = ListeTag;
