import m from "mithril";
import Notes from "../models/Notes"
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

function comment(court) {
    var active = false;
    return {
      
      view: function() {
        
        return m(".comment.italic",
          m('span.clique', {
            onclick: () => {
              console.log("clicked: active="+active);
             if(active)
             {active = false;}
             else { active = true;}
             console.log("clicked: active="+active);
            }
          }, '+'),
          m(active ? 'span.active' : 'span.hidden', m.trust(md2html(court.comment)))
        )
      }
    }
  }


  function publication(court){
  
  var optinput = {
                    name: "public",
                    type:"checkbox",
                    onchange:()=>{
                      if(court.public){
                        console.log(court);
                        Notes.unmakePublic(court._id);
                      }
                      else{
                        Notes.makePublic(court._id);  
                      }
                    }
                  }
  if(court.public){optinput["checked"]=null;}
  return   m("span.publication",
                m(".checkbox",
                  [m("input",optinput),
                  m("label",{for:"public"},"publié")]
                )
            );
  }
  
  function contenu(court) {
    //console.log(court)
    var trid = court.type == "trident" 
    return [
      //m(".forme",sonnet.forme),
      m(".poeme.block",
        //court.title!=""?m(".titre",court.title):null,
        m(".strophe", court.content.split(/\r?\n/)
          .map((v, i) => m('p.vers' + ((trid && (i != 1)) ? '.plus' : ''), ((trid && (i == 1)) ?
            '⊗ ' : '') + v)))),
      m(comment(court))
    ]
  }

function DbView() {
  //Notes.getDb();
  var params ={id:'',mdp:'',url:''};
  var error="";
  var loaded = false;
  return {
    view: function() {
      //   console.log("liste: ",Object.values(Notes.liste));
      return [
        m("div#paramsForm",
          m("#param-id",m(".form-label",'id:'),
          m("input[type=text].form-input.my-1", {
               value: params.id||"",
               oninput: (e) => {
                 e.preventDefault();
                 params.id = e.target.value;
               }
             })),
          m("#param-mdp",m(".form-label",'mot de passe:'),
          m("input[type=password].form-input.my-1", {
               value: params.mdp||"",
               oninput: (e) => {
                 e.preventDefault();
                 params.mdp = e.target.value;
               }
             })),
          m("#param-url",m(".form-label",'url:'),
          m("input[type=text].form-input.my-1", {
               value: params.url||"",
               oninput: (e) => {
                 e.preventDefault();
                 params.url = e.target.value;
               }
             })),
          m("#param-button",m("button.button",{onclick:()=>{Notes.getDb(params).catch((err)=>error=JSON.stringify(err)).then(()=>m.redraw())}},"Charger") 
          )        
        ),
        m("#error",error.length>0?error:null),
        m("ul",
        m("li",m("b","db")),
        Notes.dbList
        .map( 
          (s,i) => m('li',JSON.stringify(s))
        ),
        
        m("li",m("b","publicdb")),
        Notes.publicDbList
        .map( 
          (s,i) => m('li',i+". "+JSON.stringify(s))
        ),
        m("li",m("b","rdb")),
        Notes.rdbList
        .map( 
          (s,i) => m('li',JSON.stringify(s))
        ),
        
        m("li",m("b","rpublicdb")),
        Notes.rpublicDbList
        .map( 
          (s,i) => m('li',i+". "+JSON.stringify(s))
        )
        
        )]
            
    }
  }
};
module.exports = DbView;
