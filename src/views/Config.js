import m from "mithril";
import Notes from "../models/Notes"

import Stream from "mithril-stream";


function Config() {
  
  var config = Notes.config || {url:'',nom:'',mdp:'',titre:'Carnet',urlpublic:'',specialTags:[]};
  var sTags = Stream(config.specialTags.join(', '));

  return {
    view: function(vnode) {
      
        return m(".container.is-max-desktop.box",
          [
           m(".form-label",'Base de données:'),
           m("input[type=text].form-input.my-1", {
                value: config.url,
                oninput: (e) => {
                  e.preventDefault();
                  config.url = e.target.value;
                }
              }),
           m(".form-label",'Identifiant:'),
           m("input[type=text].form-input.my-1", {
                value: config.nom,
                oninput: (e) => {
                  e.preventDefault();
                  config.nom = e.target.value;
                }
              }),
            m(".form-label",'Mot de passe:'),
           m("input[type=password].form-input.my-1", {
                value: config.mdp,
                oninput: (e) => {
                  e.preventDefault();
                  config.mdp = e.target.value;
                }
          }),
          m(".form-label",'Titre:'),
          m("input[type=text].form-input.my-1", {
                value: config.titre,
                oninput: (e) => {
                  e.preventDefault();
                  config.titre = e.target.value;
                }
              }),
              m(".form-label",'Tags spéciaux (affichés dans le menu):'),
              m("input[type=text].form-input.my-1", {
                   value: sTags(),
                   oninput: (e) => {
                     sTags(e.target.value);
                   }
                 }),
            m(".form-label",'Base de données publique:'),
           m("input[type=text].form-input.my-1", {
                value: config.urlpublic||"",
                oninput: (e) => {
                  e.preventDefault();
                  config.urlpublic = e.target.value;
                }
              }),
            m("button.button",{onclick:()=>{config.specialTags = sTags().split(',').map((t)=>t.trim());console.log(config);Notes.saveConfig(config)}},"Enregistrer") 
          ]
          )
        }
      }
    }
  
  
  
  module.exports = Config
