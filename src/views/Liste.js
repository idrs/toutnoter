import m from "mithril";
import Notes from "../models/Notes"
import Visu from "./Visu"
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

const debug = false;


function Liste(vnode) {
  console.log("vnode.attrs",vnode.attrs);
  var tag = vnode.attrs.tagName;
  console.log("tag from Liste: ",tag)
  
  var visible = 20;
  return {
    oninit:function(){
      if(!Notes.liste || Notes.liste.length == 0){
        console.log("loadNotes from Liste");
        return Notes.loadNotes().then(()=>{console.log("layout init end");m.redraw()})}
    },
    view: function(vnode) { 
      var notesListe = Object.values(Notes.liste);
      console.log("vnode.attrs",vnode.attrs);
   tag = vnode.attrs.tagName;
  console.log("tag from Liste: ",tag)
      //console.log(Object.keys(notesListe))
     if(tag){
       // console.log("il y a tag !")
        notesListe = notesListe.filter((note)=>note.tags.includes(tag))
    }
  //console.log("longueur liste:",notesListe.length)
      return m(".container.card-contenant",m(".sansqualite",
        notesListe.length==0?m(".empty.carte-note",m("p.empty-title","Aucune note")):notesListe.reverse().map((note)=>m(".card.m-1.carte-note",{onclick:()=>m.route.set("/note/"+note._id)},m(Visu,{id:note._id})))
      ))
    }
  }
};
module.exports = Liste;
