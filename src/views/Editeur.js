import m from "mithril";
import Notes from "../models/Notes"
import Stream from "mithril-stream";
//import range from 'underscore/modules/range';


import throttle from 'underscore/modules/throttle';
//import debounce from 'underscore/modules/debounce';



function Editeur(vnode) {
  console.log("Entrer dans Editeur",vnode.attrs)
  var id = vnode.attrs.id;
  console.log("id=",id)
  var note = Notes.liste[id];
  //vnode.attrs.note = note;
  var titre = Stream(note.title);
  var texte = Stream(note.content);
  var tags = Stream(note.tags.filter((tag)=>!Notes.config.specialTags.includes(tag)).join(", "));
  
  var specialTagsStreams = Notes.config.specialTags.reduce((acc,t)=>{acc[t]=Stream(note.tags.includes(t));return acc},{});
  //console.log(specialTagsStreams)
  var specialTags = Stream.merge(Object.values(specialTagsStreams));
  function enregistre(){
    console.log("fonction enregistre");
    Notes.liste[id].title = titre();
    Notes.liste[id].content = texte();
    Notes.liste[id].tags = Notes.config.specialTags
      .filter((t)=>specialTagsStreams[t]())
      .concat(tags()
        .split(',')
        .map((t)=>t.trim())
      );
      console.log("tags enregistrés:",Notes.liste[id].tags)
    Notes.putNotes(id);    
  }

  var enregistreThrottled = throttle(enregistre,1000);
  var changes = Stream.merge([titre,texte,tags,specialTags]).map(function(){enregistreThrottled();return true;});
  //var sauvegardes = Stream.combine((a,b)=>,changes);
  changes(false);
 /* function toggleTag(t){
    var sTagArray = specialTags(); 
    if(sTagArray.includes(t)){
      specialTags(sTagArray.filter((e)=>!(e==t)));
    }
    else{
      sTagArray.push(t);
      specialTags(sTagArray);
    }
  }*/
  return {
    
    view: function(vnode) {
      //console.log(vnode.attrs);
      if (Object.values(Notes.liste).length == 0 || Notes.liste[vnode.attrs
          .id] === undefined) {
        setTimeout(m.redraw, 300);
        /*if (Notes.liste[id] === undefined) {
          console.log("is undefine ?")
        }*/
        return m(".enAttente.section", "En attente de chargement...")
      }
      else {

        //console.log(texte(),titre(),changes())
        //console.log("specialTags",Notes.specialTags.map((t)=>t+specialTagsStreams[t]()))
        return m("#editeur",[
          /*m('.btn#boutonSave',{
            onclick:()=>{ 
              changes(false);
              enregistre();
            }
          },changes()?"Enregistrer":"Rien à enregistrer" ),*/
          m("#titre", 
            m("input[type=text].form-input.text-bold.text-large.my-1", {
              oninput: function (e) { titre(e.target.value); },
              value: titre()
            })
          ),
          m("span#specialTags",Notes.config.specialTags.map(function(t){
            return m("span.chip"+(specialTagsStreams[t]()?".active":""),{
                onclick:()=>specialTagsStreams[t](!specialTagsStreams[t]())
              },
            t)
          })),
          m("span#tags",m("input[type=text].form-input.my-1", {
            placeholder: "tags",
            oninput: function (e) { tags(e.target.value) },
            value: tags()
          })
          ),
          m("#texte", 
            m("textarea#areatexte.form-input.my-1", {
              oninput: function (e) { texte(e.target.value);console.log(e.target.selectionStart) },
              
              value: texte()
            })
          ),
          m("#parent",)
        ])
      }
    }
  }
}
  
  
  module.exports = Editeur
