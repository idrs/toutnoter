import m from "mithril";
import Notes from "../models/Notes"
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

function comment(court) {
    var active = false;
    return {
      
      view: function() {
        
        return m(".comment.italic",
          m('span.clique', {
            onclick: () => {
              console.log("clicked: active="+active);
             if(active)
             {active = false;}
             else { active = true;}
             console.log("clicked: active="+active);
            }
          }, '+'),
          m(active ? 'span.active' : 'span.hidden', m.trust(md2html(court.comment)))
        )
      }
    }
  }


  function publication(court){
  
  var optinput = {
                    name: "public",
                    type:"checkbox",
                    onchange:()=>{
                      if(court.public){
                        console.log(court);
                        Notes.unmakePublic(court._id);
                      }
                      else{
                        Notes.makePublic(court._id);  
                      }
                    }
                  }
  if(court.public){optinput["checked"]=null;}
  return   m("span.publication",
                m(".checkbox",
                  [m("input",optinput),
                  m("label",{for:"public"},"publié")]
                )
            );
  }
  
  function contenu(court) {
    //console.log(court)
    var trid = court.type == "trident" 
    return [
      //m(".forme",sonnet.forme),
      m(".poeme.block",
        //court.title!=""?m(".titre",court.title):null,
        m(".strophe", court.content.split(/\r?\n/)
          .map((v, i) => m('p.vers' + ((trid && (i != 1)) ? '.plus' : ''), ((trid && (i == 1)) ?
            '⊗ ' : '') + v)))),
      m(comment(court))
    ]
  }

function Debug() {
  function versDate(n) {
    var options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    };
    const d = new Date(n);
    return d.toLocaleDateString("fr-FR", options);
  }

  
  
      var visible = 10;
  return {
    view: function() {
      //   console.log("liste: ",Object.values(Notes.liste));
      return m("ul",{onscroll:()=>{console.log("scrolling !");}},
        Object.values(Notes.liste)
        .map((s,i)=>[s,i]).reverse().slice(0,visible).map((arr) =>
            m('li.box',
              m(m.route.Link,
                {
                  selector:'div',
                  class:"bold",
                  href:"/court/" + arr[0]._id}, 
                  (arr[1] + 1) + ". " + arr[0].title
              ),
              publication(arr[0]),
              contenu(arr[0]),
              m(".italic.small", versDate(parseInt(arr[0]._id, 16))),
             // m("span.bold", arr[0].type || "?"),
              //m("div",s.content)
            )),
            Object.values(Notes.listepub)
        .map((s,i)=>[s,i]).reverse().slice(0,visible).map((arr) =>
            m('li.box',
              m(m.route.Link,
                {
                  selector:'div',
                  class:"bold",
                  href:"/court/" + arr[0]._id}, 
                  (arr[1] + 1) + ". " + arr[0].title
              ),
              publication(arr[0]),
              contenu(arr[0]),
              m(".italic.small", versDate(parseInt(arr[0]._id, 16))),
             // m("span.bold", arr[0].type || "?"),
              //m("div",s.content)
            )),
            (Object.values(Notes.liste).length>visible)?
              m("li.box",{onclick:()=>{visible=visible+10}},m(".bold","Suivants")):
                null
              )
    }
  }
};
module.exports = Debug;
