import m from "mithril";
import Notes from "../models/Notes"
import {
  marked
}
from "marked";
import dompurify from "dompurify";

function md2html(txt) {
  return dompurify.sanitize(marked.parse(txt));
}

const debug = false;


function ListePartielle(vnode) {
  //console.log("vnode.attrs",vnode.attrs);
  var stag = vnode.attrs.specialTag;

  //console.log("tag from Liste: ",stag)
  
  var visible = 3;
  return {
    view: function() {
      var notesListe = Notes.getListe();
    console.log(Notes.liste)
     console.log(notesListe)
     if(stag){
        notesListe = notesListe.filter((note)=>note.tags.includes(stag))
    }
  console.log("longueur liste:",notesListe.length)
      return m(".container",
        notesListe.map((note)=>m(".card",[m("a.is-bold",{href:"/note/"+note._id},note.title)]))
      )
    }
  }
};
module.exports = ListePartielle;
