import m from 'mithril';
//import Stream from 'mithril-stream';
import Accueil from "./views/Accueil";
import Layout from "./views/Layout";
//import Liste from "./views/Liste";
import ListeTag from "./views/ListeTag";
import TagView from "./views/TagView";
import Config from "./views/Config";
import Note from "./views/Note";
import Lapo from "./views/Lapo";
import Apropos from "./views/Apropos";


//import Debug from "./views/Debug";
import DbView from "./views/DbView";

//console.log('Hello world!');

//m.route.prefix = '';

m.route(document.getElementById('app'), "/", {
   "/":  { render: function(vnode){
     return m(Layout, m(Accueil))}
    },
   "/note/:id":  { render: function(vnode){
     return m(Layout, m(Note,vnode.attrs))}
    },
    "/lapo/:id":  { render: function(vnode){
      return m(Layout, m(Lapo,vnode.attrs))}
     },
    "/config":  { render: function(vnode){
     return m(Layout, m(Config))}
    },
    "/tag/:adress...": {render: function(vnode)
        {
            return m(Layout,m(TagView,vnode.attrs))
        }
    },
    "/tags": {render : function(vnode)
        {
          return m(Layout,m(ListeTag));
        }
    },
    "/apropos": {render : function(vnode)
        {
          return m(Layout,m(Apropos));
        }
    },
    "/db": {render : function(vnode)
        {
          return m(Layout,m(DbView));
        }
    }
});

