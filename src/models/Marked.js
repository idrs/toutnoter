import {
    marked
  }
  from "marked";
  import fm from 'front-matter';
  
  import dompurify from "dompurify";
  
  // Override function
  function preprocess(markdown) {
    const { attributes, body } = fm(markdown);
    for (const prop in attributes) {
      if (prop in this.options) {
      this.options[prop] = attributes[prop];
      }
    }
    return body;
  }
  
//marked.use({ hooks: { preprocess } });
  


const poeme = {
  name: 'poeme',
  level: 'block',                                     // Is this a block-level or inline-level tokenizer?
  start(src) { return src.match(/^:[^>]/)?.index; }, // Hint to Marked.js to stop and check for a match
  tokenizer(src, tokens) {
    const rule = /^:(?:\s*\{([^\n]+?)\})?([^\n]*)\n((?:[^\n]*\n)+?):>(?:$|\n)/;    // Regex for the complete token, anchor to string start
    const match = rule.exec(src);
    //console.log("match",match)
    if (match) {
      const token = {                                 // Token to generate
        type: 'poeme',                      // Should match "name" above
        raw: match[0],                                // Text to consume from the source
        classes: match[1]?match[1].trim():'',
        text: match[3],
        title: (match[2]?match[2].trim():""),                        // Additional custom properties
        tokens: []                                    // Array where child inline tokens will be generated
      };
      //console.log("poeme tokenizer")
      var lines = token.text.split('\n');
      var emptyLine = lines.pop(); 
      token.tokens = lines.map((line)=>{
        const prefix = /^(?:\{([^\n]+?)\}[ ]?)?(.*)/;
        const match = prefix.exec(line);
        const versToken = {
          type: 'vers',
          raw: line,
          text: match[2],
          classes:match[1]?match[1].trim():'',
          tokens:[]
        }
        this.lexer.inline(versToken.text, versToken.tokens);
        return versToken;
      });    // Queue this data to be processed for inline tokens
      
      return token;
    }
  },
  renderer(token) {
    var classes = token.classes?`class="${token.classes}"`:""
    return `<div class="poeme">\n<div class="titre-poeme">${token.title}</div>\n<div ${classes}>\n${this.parser.parseInline(token.tokens)}</div>\n</div>\n`; // parseInline to turn child tokens into HTML
  }
};

const vers = {
  name: 'vers',
  level: 'inline',                                     // Is this a block-level or inline-level tokenizer?
  renderer(token) {
    var  addClasses = token.classes?(" "+token.classes):"";
    return `  <p class="vers${addClasses}">${this.parser.parseInline(token.tokens)}</p>\n`; // parseInline to turn child tokens into HTML
  }
};


function walkTokens(token){
  console.log(token.type+": "+token.text)

}

marked.use({ extensions: [poeme,vers]});


module.exports = marked