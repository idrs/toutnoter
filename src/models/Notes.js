import m from 'mithril';
import Stream from 'mithril/stream';
//import merge from 'mergerino' 
import PouchDB from 'pouchdb'
import throttle from 'underscore/modules/throttle';


// model part

var db = new PouchDB('znotes');



//REPLICATION publique
/*
var publicdb = new PouchDB('zpublic');

db.replicate.to(publicdb, {
    live: true,
    retry: true,
    filter: function(doc) {
        console.log(doc._id,doc.public)
        return doc.public;
    }
}).then(function(result) {
  console.log("completed replication:",result)
    // handle 'completed' result
}).catch(function(err) {
    console.log(err);
});
*/


const Notes = {
    loaded: false,
    /*config: {
        url:'',
        nom:'',
        mdp:'',
        specialTags:["sur le vif"],
        urlpublic:''
    },*/
    loadNote: function(id){
        Notes.note = {};
        //console.log(id,Notes.liste[id]);
        //console.log("conf",Notes.config);
        Notes.note.id = id;
        Notes.note.mode = "view";
        Notes.note.titre = Stream(Notes.liste[id].title);
        Notes.note.texte = Stream(Notes.liste[id].content);
        if(Notes.note.texte().trim()==""){
            Notes.note.mode = "edit";
        }
        Notes.note.tags = Stream(Notes.liste[id].tags.filter((t)=>!Notes.config.specialTags.includes(t)).join(", "));
        Notes.note.specialTagsStreams = {};
        Notes.config.specialTags.forEach((t)=>{Notes.note.specialTagsStreams[t]=Stream(Notes.liste[id].tags.map((str)=>str.trim()).includes(t))});
        Notes.note.parent = Stream(Notes.liste[id].parent);
        Notes.note.enfants = Stream(Notes.liste[id].enfants);
        Notes.note.modif = Stream(Notes.liste[id].modif);
        var saver = throttle(Notes.verseNote,1000);
        var auxStream = Stream.merge(Object.values(Notes.note.specialTagsStreams));
        Notes.note.changes = Stream.lift(saver,Notes.note.titre,Notes.note.texte,Notes.note.tags,auxStream,Notes.note.parent,Notes.note.enfants);
    },
    verseNote: function() {
        //console.log("putNotes");
        //console.log("bulk launched")
        var id = Notes.note.id;
        var d = new Date();
        console.log("verseNote",id)
        Notes.liste[id].title = Notes.note.titre();
        Notes.liste[id].content = Notes.note.texte();
        Notes.liste[id].parent = Notes.note.parent();
        Notes.liste[id].enfants = Notes.note.enfants();
        Notes.liste[id].tags = Notes.config.specialTags.filter((t)=>Notes.note.specialTagsStreams[t]()).concat(Notes.note.tags().split(",").map((t)=>t.trim()).filter((t)=>t!="")); 
        
        Notes.liste[id].modif = d.getTime();
        return db.put(Notes.liste[id])
            .then((result) => {
                //console.log(result.ok);
                return db.get(id);
            })
            .then((doc)=>{console.log(doc);Notes.liste[doc._id]=doc})
            .catch((err) => console.log(err));
    }, 
    toggleMode: function(){
      if(Notes.note && Notes.note.mode){
        if(Notes.note.mode =="view"){
            Notes.note.mode = "edit";
        }
        else{
            Notes.note.mode = "view";
        }
      }  
      console.log("toggleMode-> Notes.note:",Notes.note)
    },
    loadConfig: function() {
        Notes.config = JSON.parse(localStorage.getItem('znotes-config')) ||  {
            url:'',
            nom:'',
            mdp:'',
            titre:"Carnet",
            specialTags:["sur le vif"],
            urlpublic:''
        };
        console.log(Notes.config)
        if(Notes.config && !Notes.config.urlpublic){Notes.config["urlpublic"]="";}
    },
    saveConfig: function(config) {
        Notes.specialTags = config.specialTags;
        localStorage.setItem('znotes-config', JSON.stringify(config));
        loadRemoteDB();
    },
    //onview: 0,
    loadNotes: function(redraw) {
        /*console.log("loadNotes from Notes");
        Notes.load = (Notes.load || 0)+1;
        console.log("nb chargement: "+Notes.load)
        var load = (localStorage.getItem('load') || 0)+1; 
        localStorage.setItem('load',load)
        console.log("LocalStorageLoad="+load);*/ 
        Notes.loaded = false;
        Notes.liste = Notes.liste || {};
        return db.allDocs({
                include_docs: true
            })
            .then((result) => {
                //console.log("load results: ",result);
                result.rows.forEach((r) => {
                    Notes.liste[r.id] = Notes.checkNote(r.doc);

                });
                console.log("loading finished")
                Notes.loaded = true;
                if (redraw) {
                    m.redraw()
                }
            })
            .catch((err) => console.log(err));
    /*    publicdb.allDocs({
                include_docs: true
            })
            .then((result) => {
                //console.log("load results: ",result);
                result.rows.forEach((r) => {
                    if(!r.id.startsWith('_design')){Notes.listepub[r.id] = r.doc}
                });
                
                if (redraw) {
                    m.redraw()
                }
            })
            .catch((err) => console.log(err));
    */
    },
    putNotes: throttle(function(id) {
        //console.log("putNotes");

        //console.log("bulk launched")
        db.put(Notes.liste[id])
            .then((result) => {
                //console.log(result.ok);
                return db.get(id);
            })
            .then((doc)=>{console.log(doc);Notes.liste[doc._id]=doc})
            .catch((err) => console.log(err));
    }, 1000),
    //liste: this.liste || {},
    getListe: function(){
        return Object.values(this.liste);
    },
    listepub: {},
    //specialTags: Notes.specialTags || ['sur le vif'],
    loadPublicDb: function() {
        //ajouter obfuscation adresse
        var remotePublicDB = new PouchDB('https://' + Notes.config.nom + ':' + Notes.config.mdp + '@' + Notes.config.urlpublic);

        publicdb.sync(remotePublicDB, {
            live: true,
            retry: true
        }).on('change', function(change) {
            // yo, something changed!
            console.log("publicdb change:",change);
            Notes.loadNotes(true);
        }).on('paused', function(info) {
            // replication was paused, usually because of a lost connection
            Notes.log("paused sync", info)
        }).on('active', function(info) {
            // replication was resumed
            Notes.log("resumed sync", info)
        }).on('error', function(err) {
            // totally unhandled error (shouldn't happen)
            Notes.log("error sync, problem!", err)
        });
    },
    checkNote: function(n) {
        n.title = n.title || "";
        n.tags = n.tags || [];
        n.content = n.content || "";
        return n;
    },
    nouvelleNote: function(options) {
        // creer un nouveau poeme
        var s = {}
        var d = new Date();
        s._id = d.getTime().toString(16);
        s.title = options.title || '';
        s.parent = options.parent || '';
        s.enfants = [];
        s.tags = options.tags || [];
        s.content = '';
        s.comment = '';
        s.public = false;
        s.creee = d.getTime();
        s.modif = s.creee;
        //console.log('type',type)
        //console.log(Notes)

        console.log(s)
        return db.put(s).then((rep) => {
            //console.log(rep);
            //Notes.loadNotes(true)
            return db.get(s._id);
        })
        .then((doc)=>{console.log("Note sauvée ?");Notes.liste[doc._id]=doc;return doc._id})
        .catch((err) => console.log(err));
        
        //Notes.liste[s._id]=s;
        //Notes.putNotes(s);
        //this.liste[s._id]=s;
        
    },
    effaceNote: function(options){
        var id = Notes.note.id;
        var d = new Date();
        console.log("effaceNote",id)
        Notes.liste[id].title = Notes.note.titre();
        Notes.liste[id].content = Notes.note.texte();
        Notes.liste[id].parent = Notes.note.parent();
        Notes.liste[id].enfants = Notes.note.enfants();
        Notes.liste[id].tags = Notes.config.specialTags.filter((t)=>Notes.note.specialTagsStreams[t]()).concat(Notes.note.tags().split(",").map((t)=>t.trim())); 
        Notes.liste[id].modif = d.getTime();
        //!!!
        Notes.liste[id]._deleted = true;
        return db.put(Notes.liste[id])
            .then((result) => {
                //console.log(result.ok);
                Notes.liste[id]=null;
                m.route.set("/")
            })
            .catch((err) => console.log(err));
    },
    togglePublic: function(id) {
        console.log("toggle", id)
        if (Notes.liste[id].public) {
            Notes.liste[id].public = false;
        } else {
            Notes.liste[id].public = true;
        }
        Notes.putNotes(id);
        if (Notes.liste[id].public) {
            console.log("poeme publié")
        } else {
            console.log("tentative d'effacement")
            publicdb.get(id).then((doc) => {
                console.log('remove public', doc)
                publicdb.remove(doc).then(() => {
                    Notes.loadNotes(true)
                });
            }).catch(err => Notes.log("erreur après remove public", err))
        }

    },
    update: function(id, partie, texte) {
        Notes.liste[id][partie] = texte;
    },
    delete: function(id) {
        //delete Notes.liste[id];
        db.get(id).then((doc) => {
            db.remove(doc);
        }).catch(err => Notes.log("erreur après remove", err))
    },
    logfile: [],
    log: function(...logText) {
        var d = new Date();
        Notes.logfile.push(d + ':\n' + logText.join(' '))
    },
    dbList:[],
    publicDbList:[],
      rdbList : [],
      rpublicDbList : [],
    getDb: function(params){
      console.log("calling getDB");
      var importDB = new PouchDB('https://' + params.id + ':' + params.mdp + '@' + params.url);
        dbList = [];
      
     return importDB.allDocs({
                include_docs: true
            })
            .then((result) => {
                //console.log("load results: ",result);
                result.rows.forEach((r) => {
                    if(!Notes.liste[r.doc._id]){
                    Notes.dbList.push(r.doc);}
                    
                });
                m.redraw();
            })
            .catch((err) => console.log(err));
      
    }
}

Notes.loadConfig();

function loadRemoteDB() {
    if (Notes.config) {
        var remoteDB = new PouchDB('https://' + Notes.config.nom + ':' + Notes.config.mdp + '@' + Notes.config.url);

        db.sync(remoteDB, {
            live: true,
            retry: true
        }).on('change', function(change) {
            // yo, something changed!
            Notes.loadNotes(true);
            Pouchdb.replicate(db,publicdb, {
    live: true,
    retry: true,
    filter: function(doc) {
        console.log(doc._id,doc.public)
        return doc.public;
    }
}).then(function(result) {
  console.log("completed replication:",result)
    // handle 'completed' result
}).catch(function(err) {
    console.log(err);
});
        }).on('paused', function(info) {
            // replication was paused, usually because of a lost connection
            Notes.log("paused sync", info)
        }).on('active', function(info) {
            // replication was resumed
            Notes.log("resumed sync", info)
        }).on('error', function(err) {
            // totally unhandled error (shouldn't happen)
            Notes.log("error sync, problem!", err)
        });
        if (Notes.config.urlpublic.length > 0) {
            Notes.loadPublicDb();
        }
    }
}

loadRemoteDB();

//if(!Notes.liste){ Notes.loadNotes();}

//console.log("listepub:",Notes.listepub);

//m.redraw();

module.exports = Notes
